# Install Julia

The first step is to install [Julia](https://julialang.org) itself. Open a new termnal and go to your Downloads folder:

`cd ~/Downloads`

At the time of writing (February 2022), the current stable release is 1.7.2. You can find the link to the latest version on the [downloads page](https://julialang.org/downloads/).
If a more recent stable version is available, replace `1.7.2` everywhere in these instructions with the version that you download.
Download the latest compressed Julia binaries:

`wget https://julialang-s3.julialang.org/bin/linux/x64/1.7/julia-1.7.2-linux-x86_64.tar.gz`


Extract the `tar.gz` file:

`tar -xvzf julia-1.7.2-linux-x86_64.tar.gz`

and copy the extracted folder to `/opt/`:

`sudo cp -r julia-1.7.2 /opt/`

Finally, create a symbolic link to the julia installation inside the user folder `/usr/local/bin`:

`sudo ln -s /opt/julia-1.7.2/bin/julia /urs/local/bin/julia`

Test that the installation worked by opening a new terminal and type:

`julia`

That should open the Julia REPL.

# Install VS Code Extension